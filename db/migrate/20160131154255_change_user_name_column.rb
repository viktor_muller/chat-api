class ChangeUserNameColumn < ActiveRecord::Migration
  def change
    rename_column :users, :namerails, :name
    change_column :users, :name, :string, :unique=>true
  end
end
