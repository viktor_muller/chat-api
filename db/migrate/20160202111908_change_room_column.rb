class ChangeRoomColumn < ActiveRecord::Migration
  def change
    rename_column :messages, :roomrai_id, :room_id
  end
end
