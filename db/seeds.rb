# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

5.times do
  users = User.create([{ name: Faker::Name.name }, {name: Faker::Name.name}])
  room = Room.create(name: Faker::App.name)
  users.each do |user|
    RoomUser.create(room_id: room.id, user_id: user.id)
  end

  message = Message.create(text: Faker::Lorem.paragraph,
                           user_id: 1,
                           roomrai_id: 1)

  UnreadMessage.create(user_id: 1,
                         message_id: [1, 2, 3, 4, 5, 6, 7, 10].sample)
end