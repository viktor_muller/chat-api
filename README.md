# Примеры запросов:
| Вид запроса                                 | Запрос |
| :-------------                              |:-------------|
| Регистрация                                 | ``` $ curl -X POST -d "user[name]=Ivanov" /users.json ```|
| Авторизация                                 | ``` $ curl -X POST -d "user[name]=Ivanov" /session.json ```|
| Создание чата                               | ``` $ curl -X POST -d "chat[name]=Переписка&chat[user_ids][]=1&chat[user_ids][]=2" /chats.json ```|
| Изменение название чата или участников чата | ``` $ curl -X PUT -d "chat[name]=new name&chat[user_ids][]=2&chat[user_ids][]=4" /chats/[:id] ```|
| Написать сообщение в чат                    | ``` $ curl -X POST -d  "message[text]='message'&message[room_id]=1" /messages.json ```|
| Получение списка чатов                      | ``` $ curl -X GET /chats.json ```|
| Отметить чат как прочитанный                | ``` $ curl -X PUT -d "chat[read]=true" /chats/[:id] ```|
| Получить список непрочитанных сообщений     | ``` $ curl -X GET /messages.json ```|
| Получение списка пользователеи              | ``` $ curl -X GET /users.json ```|