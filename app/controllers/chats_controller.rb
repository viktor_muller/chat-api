class ChatsController < ApplicationController
  respond_to :json

  def index
    rooms = Room.joins(:room_users).where("room_users.user_id = #{current_user.id}")
    @roomlist = rooms.map do |room|
      users_id = []
      room.users.each{ |u| users_id.push(u.id) }
      {chat: { id: room.id, name: room.name, user_ids: users_id, unread_messages_count: current_user.unread_messages.count }}
    end
    respond_with @roomlist.to_json
  end

  def create
    @user_ids = user_ids_params

    if @user_ids.count > 1
      @room = Room.new(room_params)
      if @room.save
        @user_ids.each{ |id| @room.room_users.create(user_id: id) }
        render json: {chat: { id: @room.id, name: @room.name, user_ids: @user_ids }}
      else
        render json: {error: "Chat could not be created."}, status: 400
      end
    else
      render json: {error: "Chat could not be created."}, status: 400
    end
  end

  def update

    if params[:chat][:read].present?
      set_read_status(params[:id])
      # render json: "Messages updated", status: 201
    else
      begin
        @room = Room.where(:id => params[:id]).joins(:room_users).where("room_users.user_id = #{current_user.id}")
      rescue ActiveRecord::RecordNotFound => e
        @room = nil
      end

      unless @room.empty?
        if params[:chat][:name].present?
          @room.update(params[:id], :name => params[:chat][:name])
        end

        unless user_ids_params.empty?
          @room = Room.find(params[:id])
          @room.room_users.destroy_all
          user_ids_params.each{ |id| @room.room_users.create(user_id: id) }
        end

        user_ids = []
        @room.room_users.each{ |i| user_ids.push(i.user_id)}
        render json: {chat: { id: @room.id, name: @room.name, user_ids: user_ids }}
      else
        render json: {error: "Chat could not be updated."}, status: 500
      end
    end
  end

  private

  def set_read_status room_id
    room = Room.find(room_id)
    messages = Message.where(user_id: current_user.id, room_id: room_id)
    messages.each{ |m| @list.push(m.unread_message) }
  end

  def room_params
    params.require(:chat).permit(:name)
  end

  def user_ids_params
    if params[:chat][:user_ids].present?
      user_ids = params[:chat][:user_ids]
      user_ids.push(current_user.id)
      user_ids.uniq
    else
      []
    end
  end

end
