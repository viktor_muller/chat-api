class MessagesController < ApplicationController
  respond_to :json

  def index
    messages = Message.where(:user_id => current_user.id)
    @messages_list = messages.map do |m|
      {message: { text: m.text, room_id: m.room.id, unread: m.unread_messages.empty? }}
    end
    render json: @messages_list
  end

  def create
    room = Room.where(:id => params[:message][:room_id]).joins(:room_users).where("room_users.user_id = #{current_user.id}")
    if room.empty?
      render json: {error: "You can add message in this chat"}, status: 403
    else
      message = Message.new(:text => params[:message][:text], :room_id => params[:message][:room_id], :user_id => current_user.id )
      if message.save
        message.room.users.each{ |u| message.unread_messages.create(:user_id => u.id) }
        render json: "Message created", status: 201
      else
        render json: {error: "Something went wrong"}, status: 500
      end


    end
  end

end