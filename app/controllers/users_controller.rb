class UsersController < ApplicationController
  respond_to :json
  protect_from_forgery except: :create

  def index
    users = User.all
    respond_with users.map{ |u| prepare_json u }
  end

  def create
    new_user = User.create(user_params)

    if new_user.save
      render json: prepare_json(new_user), status: 201
    else
      render json: {error: "User could not be created."}, status: 400
    end

  end

  private

  def user_params
    params.require(:user).permit(:name)
  end

  def prepare_json u
    { user: {:id => u.id, :name => u.name, :messages_count => u.unread_messages.count} }
  end

end
