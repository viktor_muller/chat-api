class SessionController < ApplicationController
  respond_to :json
  before_action :authorize, except: :create
  protect_from_forgery with: :null_session

  def create
    @user = User.find_by(name: params[:user][:name])
    if @user
      session[:current_user_id] = @user.id
      render json: { user: {:id => @user.id, :name => @user.name, :messages_count => @user.unread_messages.count} }, status: 200
    else
      render json: {error: "Can't uthorized."}, status: 401
    end

  end
end
