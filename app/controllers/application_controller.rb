class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  # before_action :authorize, except: :create
  #
  def current_user
    User.find(1)
    # @user ||= User.find(session[:current_user_id]) if session[:current_user_id]
  end
  #
  # def authorize
  #   unless current_user
  #     render json: {error: "User unauthorized."}, status: 401
  #   end
  # end
end
