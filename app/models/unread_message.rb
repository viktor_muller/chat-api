class UnreadMessage < ActiveRecord::Base
  belongs_to :users
  belongs_to :messages
end
