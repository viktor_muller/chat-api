class User < ActiveRecord::Base
  validates :name, presence: true
  validates_uniqueness_of :name

  has_many :messages
  has_many :room_users
  has_many :rooms, through: :room_users

  has_many :unread_messages
end
