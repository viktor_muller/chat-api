class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :room
  has_many :unread_messages
end
